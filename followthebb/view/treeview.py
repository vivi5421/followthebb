import inspect
import logging
import multiprocessing
import re
import tkinter as tk
import tkinter.ttk as ttk
import traceback
import webbrowser

from .images import Images
from followthebb.model import SUCCESSFUL, IN_PROGRESS, FAILED, PENDING
from followthebb.controller import IS_NEW, IS_UPDATE, CONTINUE
from .notification import notification_stateless

logger = logging.getLogger(__name__)


def fixed_map(option, style):
    # Fix for setting text colour for Tkinter 8.6.9
    # From: https://core.tcl.tk/tk/info/509cafafae
    #
    # Returns the style map for 'option' with any styles starting with
    # ('!disabled', '!selected', ...) filtered out.

    # style.map() returns an empty list for missing options, so this
    # should be future-safe.
    return [elm for elm in style.map('Treeview', query_opt=option) if elm[:2] != ('!disabled', '!selected')]


##########
# COLUMN #
##########
class Column(object):

  MIN_WIDTH = 40
  WIDTH = 100

  def __init__(self, title, label, value, minwidth=MIN_WIDTH, width=WIDTH, fixed_sized=False):
    self.title = title
    self.label = label
    self.minwidth = min(minwidth, width)
    self.width = max(minwidth, width)
    self.fixed_sized = fixed_sized
    self.value = value

  def get(self, item):
    res = self.value(item)
    return res if res else ''






#############
# TREE VIEW #
#############
class BitBucketTreeView(ttk.Treeview):

  COLUMNS = [
    Column(title='ttype', label='Type', value=lambda x: x.item.type, width=80),
    Column(title='project', label='Project', value=lambda x: x.item.repo.project),
    Column(title='repo', label='Repo', value=lambda x: x.item.repo.repo),
    Column(title='branch', label='Branch', value=lambda x: x.item.branch.replace('refs/heads/', '')),
    Column(title='id', label='Id', value=lambda x: x.item.displayId),
    Column(title='author', label='Author', value=lambda x: x.item.author, width=90),
    Column(title='title', label='Title', value=lambda x: x.item.title, width=600),
    Column(title='status', label='Status', value=lambda x: x.item.status),
    Column(title='date', label='Date', value=lambda x: x.item.date, width=120),
    Column(title='url', label='URL', value=lambda x: x.item.url, minwidth=0, width=0, fixed_sized=True),
    Column(title='obj', label='', value=lambda x: x, minwidth=0, width=0, fixed_sized=True),
    ]

  def __init__(self, parent, args):
    super().__init__(parent, columns=[_col.title for _col in BitBucketTreeView.COLUMNS])
    
    self.style = ttk.Style()
    self.style.map('Treeview', foreground=fixed_map('foreground', self.style), background=fixed_map('background', self.style))
    self.tag_configure('highlight', background='#FFEA80')
    
    self.images = Images(args)
    self.parent = parent
    self.args = args
    self.content = []
    self.columns()
    self.bind('<Double-1>', self.browse)
    self.bind('<Button-3>', self.untag)

  def columns(self):
    self.heading('#0', text='', anchor=tk.W)
    self.column('#0', width=40, stretch=tk.NO)
    for _col in BitBucketTreeView.COLUMNS:
      self.heading(_col.title, text=_col.label)
      self.column(_col.title, minwidth=_col.minwidth, width=_col.width, anchor=tk.W, stretch=tk.NO if _col.fixed_sized else tk.YES)

  def browse(self, event):
    selected = self._selected(event)
    self.untag(event)
    logger.warning('Webbrowser for [{}]'.format(selected))
    webbrowser.open(selected.item.url, new=2)

  def untag(self, event):
    selected = self._selected(event)
    selected.is_blinking = False
    selected.is_highlighted = False
    selected.is_notified = False
    logger.warning('Untag [{}]'.format(selected))
    self.feed(self.content, notification=False)

  def _selected(self, event):
    item = self.identify('item', event.x, event.y)
    selected = self.item(item, 'values')
    return [item for item in self.content if item.item.repo.project == selected[1] and item.item.repo.repo == selected[2] and item.item.id.startswith(selected[4])][0]


  def feed(self, content, notification=True):
    self.content = content
    popupWindow = False
    self.delete(*self.get_children())
    for item in content:
      values = [_col.get(item) for _col in BitBucketTreeView.COLUMNS]
      tags = ['highlight'] if item.is_highlighted else []
      try:
        if item.item.build:
          self.insert('', 'end', text='', values=values, image=self.images.photoimage[item.item.build], tags=tags)
        else:
          self.insert('', 'end', text='', values=values, tags=tags)
        if item.is_blinking:
          popupWindow = True
        if notification and item.is_notified:
          p = multiprocessing.Process(target=notification_stateless, args=(
            '{}/{}/{}'.format(item.item.repo.project, item.item.repo.repo, item.item.branch),
            '{} - {}'.format(item.item.displayId, item.item.title),
            self.images.path[item.item.build]
          ))
          p.start()
      except AttributeError:
        traceback.print_exc()
    return popupWindow




