# To Be Done #

## Repositories ##

- [x] remove the repos.txt file by using:
- [x] study [profile/recent/repos](https://docs.atlassian.com/bitbucket-server/rest/6.10.0/bitbucket-rest.html#idp139)
- [x] study [dashboard/pull-requests](https://docs.atlassian.com/bitbucket-server/rest/6.10.0/bitbucket-rest.html#idp90)
- [x] ~~/rest/api/1.0/projects/{projectKey}/repos/{repositorySlug}/commits?&merges=only~~
- [x] ~~check repositories where user have write/admin rights~~
- [x] `--repos` to add repositories to be checked (all branches)

## List of builds ##

- [x] highlight changed status in treeview until you click on it
- [ ] improve PR state: approved, merged, new comments...
- [x] build status to be unknown when nothing is build (current is build passed)
- [x] remove icon 'pending' but do not break when displaying no picture
- [x] it looks like you may ignore merged commits since your commit appear "always" in the commit list before the merge
- [x] Support the no build status
- [ ] switch: view only my prs (filter in author?) - view all prs
- [ ] make sure to handle correctly commits done with amend (timestamp may be older)
- [ ] branch name too long in notification (refs/heads/...)
- [ ] when having more than 1 commit on same branch for a given repo, it looks like udpate on one gives hihghlight and notification on the older ones
- [ ] option I don't care for a PR

## Application ##

- [x] ~~systray icon~~
- [ ] find a way to allow the app to start with windows
- [ ] force refresh button
- [x] ~~try to use Kerberos ticket~~
- [x] use wincred (Windows Credentials to retrieve BitBucket password)
- [x] use only ico file
- [x] introduce again `--request-limit` parameter
- [ ] make sure application is always correctly closed
- [x] double-click on version displays changelog-
- [ ] Need to solve new update issue `WARNING: Retrying (Retry(total=4, connect=None, read=None, redirect=None, status=None)) after connection broken by 'SSLError(SSLCertVerificationError(1, '[SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: unable to get local issuer certificate (_ssl.c:1108)'))': /simple/followthebb/`
- [ ] make list sortable (https://stackoverflow.com/questions/1966929/tk-treeview-column-sort)
- [ ] option: do not popup
- [ ] see if we can use same logic, or atexit to log output (or always log to %TEMP%)

## Safety / Optimization ##

- [x] make sure you don't launch another scan if previous one is not finished
- [ ] Update repositories on-the-fly (instead of parsing all of them all the same time)
- [ ] try to find a way to detect if a repo has been modified since last time (keeping first id, or something like that...)
- [x] stop args.now because in case the application is running more than 1 day

## Pull Requests ##

- [ ] handle the automatic cascading pull requests/commits
- [ ] follow PRs where you are author, reviewer?, approver, ... (or case you rebuild an existing PR you are not the author)

## Packaging ##

- [ ] use pyinstaller to make executable

