import lazy_property
from PIL import Image, ImageTk

from followthebb.model import SUCCESSFUL, IN_PROGRESS, FAILED, PENDING


class Images(object):

    def __init__(self, args):
        self.args = args

    @lazy_property.LazyProperty
    def path(self):
        return {
            SUCCESSFUL: self.args.resources['success.ico'],
            FAILED: self.args.resources['failed.ico'],
            IN_PROGRESS: self.args.resources['progress.ico'],
            None: self.args.resources['blank.ico']
        }

    @lazy_property.LazyProperty
    def photoimage(self):
        return {
            SUCCESSFUL: ImageTk.PhotoImage(Image.open(self.args.resources['success.ico'])),
            FAILED: ImageTk.PhotoImage(Image.open(self.args.resources['failed.ico'])),
            IN_PROGRESS: ImageTk.PhotoImage(Image.open(self.args.resources['progress.ico'])),
            None: ImageTk.PhotoImage(Image.open(self.args.resources['blank.ico']))
        }
