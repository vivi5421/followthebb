from setuptools import setup, find_packages
import unittest

with open("README.md", "r") as fh:
    long_description = fh.read()

def my_test_suite():
    test_loader = unittest.TestLoader()
    test_suite = test_loader.discover('tests', pattern='test_*.py')
    return test_suite

setup(
    name="followthebb",
    packages=find_packages(exclude=['tests']),
    version="0.0.1",
    author="Nicolas Villard-Parriault",
    author_email="nicolas.villard-parriault@amadeus.com",
    description="Follow your BitBucket builds",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://rndwww.nce.amadeus.net/git/projects/TOOL/repos/followthebb/browse",
    python_requires='>=3.6',
    test_suite="setup.my_test_suite",
    include_package_data = True,
    package_data={
        "": ["*.ico"]
    },
    install_requires=[
        'jsonpickle',
        'lazy-property',
        'requests',
        'plyer',
        'Pillow',
        'PyYAML'
    ]
)