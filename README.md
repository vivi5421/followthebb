# README #

## FollowTheBB ##

Follow automatically all your interesting BitBucket builds.

![Scheme](docs/images/mainWindows.jpg)

- Follow automatically your BitBucket builds (based on your Pull Requests, recent repositories, and provided ones through `--repos`)
- Show notification, pop-up window and hightlight build status in case of new item or item update
- Double-click on an item to open the webpage
- Right-click on an highlighted item to reset it
- Automatic update thanks to pip
- Double-click on version to show changelog

On the bottom, you see the current version. You can also see if a scanning is in progress.

![Scheme](docs/images/notification.jpg)

Here is an example of notification you see in case there is a build status change.


## Prerequisites ##

- [Python3.6]((https://www.python.org/downloads/)) or above

## Installation ##

- Simply download [`FTBB.pyw`](https://rndwww.nce.amadeus.net/git/projects/TOOL/repos/ftbb/raw/FTBB.pyw?at=refs%2Fheads%2Fmaster)

## Command-line usage ##

```bash
usage: FTBB.py [-h] [--request-limit REQUEST_LIMIT] [--search-period SEARCH_PERIOD] [--wait WAIT] [-r [REPOS [REPOS ...]]] [--threads THREADS] [--debug | --info] [--offline]

optional arguments:
  -h, --help            show this help message and exit
  --request-limit REQUEST_LIMIT
                        Each BitBucket RestAPI call has a limit in the response. This is the limitation. Bigger numbers makes having less calls, but more data to process. Because of this and because you cannot provide date
                        as input in the requests, you may have more answers than expected regarding the search-period option. (default: 1000)
  --search-period SEARCH_PERIOD
                        Number of days before today you want to retrieve commits and pull requests from. COndition is 'at least', you may have some commits/pull requests earlier to this limit (cf request-limit option).
                        (default: 4)
  --wait WAIT           Time, in seconds, between 2 scans of the repositories to find new builds and new build statuses. (default: 60)
  -r [REPOS [REPOS ...]], --repos [REPOS [REPOS ...]]
                        Repositories you want to check as well, in the format "project/repo" (default: [])
  --threads THREADS     How many threads can be running in parallel. 0 for sequential execution (default: 50)
  --debug               Allow debug logs (default: False)
  --info                Allow info logs (default: False)
  --offline             Do not try to update followthebb module, use the local one directly (default: False)
```

## Source ##

[BitBucket](https://rndwww.nce.amadeus.net/git/projects/TOOL/repos/followthebb/browse)

## Contact ##

[Nicolas Villard-Parriault](mailto:nicolas.villard-parriault@amadeus.com)

## Other links ##

[CHANGELOG](https://rndwww.nce.amadeus.net/git/projects/TOOL/repos/followthebb/browse/CHANGELOG.md)

[TO BE DONE](https://rndwww.nce.amadeus.net/git/projects/TOOL/repos/followthebb/browse/TO_BE_DONE.md)