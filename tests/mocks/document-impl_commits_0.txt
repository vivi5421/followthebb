https://rndwww.nce.amadeus.net/git/rest/api/1.0/projects/bc/repos/document-impl/commits?start=0

200
{
  "isLastPage": false,
  "limit": 25,
  "nextPageStart": 25,
  "size": 25,
  "start": 0,
  "values": [
    {
      "author": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "authorTimestamp": 1583399763000,
      "committer": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "committerTimestamp": 1583399763000,
      "displayId": "0c1cdba2210",
      "id": "0c1cdba2210a34f00dd3ea655f58ab866282040c",
      "message": "Automatic merge from release/6.15 -> develop\n\n* commit 'c75df1b2d65b651d93d201ea5af9b907a3a3a4d4':\n  PTR 18858528 [Medium]: ARD: One fareComponent in Verb call ttstuq",
      "parents": [
        {
          "displayId": "6dd30002509",
          "id": "6dd3000250973bce7a593bffd18c75133a96f3aa"
        },
        {
          "displayId": "c75df1b2d65",
          "id": "c75df1b2d65b651d93d201ea5af9b907a3a3a4d4"
        }
      ],
      "properties": {
        "records": [
          "PTR 18858528"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "authorTimestamp": 1583399762000,
      "committer": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "committerTimestamp": 1583399762000,
      "displayId": "c75df1b2d65",
      "id": "c75df1b2d65b651d93d201ea5af9b907a3a3a4d4",
      "message": "Merge pull request #549 in BC/document-impl from ~JEAN.CHAUBET/document-impl:feature/ARD-16256-PTR-18858528-ARD-Missing-One-fareComponent-in-Verb-call-ttstuq to release/6.15\n\n* commit 'd1fbfcfacb36b3c968210c21e7c861715cb4bca7':\n  PTR 18858528 [Medium]: ARD: One fareComponent in Verb call ttstuq",
      "parents": [
        {
          "displayId": "6dd30002509",
          "id": "6dd3000250973bce7a593bffd18c75133a96f3aa"
        },
        {
          "displayId": "d1fbfcfacb3",
          "id": "d1fbfcfacb36b3c968210c21e7c861715cb4bca7"
        }
      ],
      "properties": {
        "jira-key": [
          "ARD-16256"
        ],
        "records": [
          "PTR 18858528"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Jean-Baptiste CHAUBET",
        "emailAddress": "jean-baptiste.chaubet@amadeus.com",
        "id": 32092,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/jean.chaubet"
            }
          ]
        },
        "name": "jean.chaubet",
        "slug": "jean.chaubet",
        "type": "NORMAL"
      },
      "authorTimestamp": 1583329649000,
      "committer": {
        "active": true,
        "displayName": "Jean-Baptiste CHAUBET",
        "emailAddress": "jean-baptiste.chaubet@amadeus.com",
        "id": 32092,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/jean.chaubet"
            }
          ]
        },
        "name": "jean.chaubet",
        "slug": "jean.chaubet",
        "type": "NORMAL"
      },
      "committerTimestamp": 1583329649000,
      "displayId": "d1fbfcfacb3",
      "id": "d1fbfcfacb36b3c968210c21e7c861715cb4bca7",
      "message": "PTR 18858528 [Medium]: ARD: One fareComponent in Verb call ttstuq",
      "parents": [
        {
          "displayId": "6dd30002509",
          "id": "6dd3000250973bce7a593bffd18c75133a96f3aa"
        }
      ],
      "properties": {
        "records": [
          "PTR 18858528"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Frederic LARUELLE",
        "emailAddress": "flaruelle@amadeus.com",
        "id": 40745,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/flaruelle"
            }
          ]
        },
        "name": "flaruelle",
        "slug": "flaruelle",
        "type": "NORMAL"
      },
      "authorTimestamp": 1582878735000,
      "committer": {
        "active": true,
        "displayName": "Frederic LARUELLE",
        "emailAddress": "flaruelle@amadeus.com",
        "id": 40745,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/flaruelle"
            }
          ]
        },
        "name": "flaruelle",
        "slug": "flaruelle",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582878735000,
      "displayId": "6dd30002509",
      "id": "6dd3000250973bce7a593bffd18c75133a96f3aa",
      "message": "Automatic merge from release/6.11 -> develop\n\n* commit '5ba5e2fe586d571a55ed6f626be2a1d03116b711':\n  PTR 99999999 - WHITE-2615 Automated cascading of document-api/-impl patches to aere-xml",
      "parents": [
        {
          "displayId": "82aa21b2814",
          "id": "82aa21b281472b534211fabdb5190c2f3d1da330"
        },
        {
          "displayId": "5ba5e2fe586",
          "id": "5ba5e2fe586d571a55ed6f626be2a1d03116b711"
        }
      ],
      "properties": {
        "jira-key": [
          "WHITE-2615"
        ],
        "records": [
          "PTR 99999999"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Frederic LARUELLE",
        "emailAddress": "flaruelle@amadeus.com",
        "id": 40745,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/flaruelle"
            }
          ]
        },
        "name": "flaruelle",
        "slug": "flaruelle",
        "type": "NORMAL"
      },
      "authorTimestamp": 1582878734000,
      "committer": {
        "active": true,
        "displayName": "Frederic LARUELLE",
        "emailAddress": "flaruelle@amadeus.com",
        "id": 40745,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/flaruelle"
            }
          ]
        },
        "name": "flaruelle",
        "slug": "flaruelle",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582878734000,
      "displayId": "5ba5e2fe586",
      "id": "5ba5e2fe586d571a55ed6f626be2a1d03116b711",
      "message": "Merge pull request #547 in BC/document-impl from ~BRICHARD/document-impl:WHITE-2615 to release/6.11\n\n* commit '9c5556ba9a540e15e4e6687c090fdd35eb8fb572':\n  PTR 99999999 - WHITE-2615 Automated cascading of document-api/-impl patches to aere-xml",
      "parents": [
        {
          "displayId": "36bcc873315",
          "id": "36bcc873315a51be43c8d323cf92e723738e4ae2"
        },
        {
          "displayId": "9c5556ba9a5",
          "id": "9c5556ba9a540e15e4e6687c090fdd35eb8fb572"
        }
      ],
      "properties": {
        "jira-key": [
          "WHITE-2615"
        ],
        "records": [
          "PTR 99999999"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Bertrand RICHARD",
        "emailAddress": "bertrand.richard@amadeus.com",
        "id": 20914,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/brichard"
            }
          ]
        },
        "name": "brichard",
        "slug": "brichard",
        "type": "NORMAL"
      },
      "authorTimestamp": 1582876248000,
      "committer": {
        "active": true,
        "displayName": "Bertrand RICHARD",
        "emailAddress": "bertrand.richard@amadeus.com",
        "id": 20914,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/brichard"
            }
          ]
        },
        "name": "brichard",
        "slug": "brichard",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582876248000,
      "displayId": "9c5556ba9a5",
      "id": "9c5556ba9a540e15e4e6687c090fdd35eb8fb572",
      "message": "PTR 99999999 - WHITE-2615 Automated cascading of document-api/-impl patches to aere-xml",
      "parents": [
        {
          "displayId": "36bcc873315",
          "id": "36bcc873315a51be43c8d323cf92e723738e4ae2"
        }
      ],
      "properties": {
        "jira-key": [
          "WHITE-2615"
        ],
        "records": [
          "PTR 99999999"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "swb2-izu-BC",
        "emailAddress": "swb2-izu-BC@amadeus.com",
        "id": 142345,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/swb2-izu-bc"
            }
          ]
        },
        "name": "swb2-izu-BC",
        "slug": "swb2-izu-bc",
        "type": "NORMAL"
      },
      "authorTimestamp": 1582642722000,
      "committer": {
        "active": true,
        "displayName": "swb2-izu-BC",
        "emailAddress": "swb2-izu-BC@amadeus.com",
        "id": 142345,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/swb2-izu-bc"
            }
          ]
        },
        "name": "swb2-izu-BC",
        "slug": "swb2-izu-bc",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582642722000,
      "displayId": "82aa21b2814",
      "id": "82aa21b281472b534211fabdb5190c2f3d1da330",
      "message": "PTR 99999999 - Groovy Bumper - bumping version",
      "parents": [
        {
          "displayId": "00229edb226",
          "id": "00229edb226bfea6e7c7a16abd6bf3126f9c427a"
        }
      ],
      "properties": {
        "records": [
          "PTR 99999999"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "authorTimestamp": 1582642684000,
      "committer": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582642684000,
      "displayId": "00229edb226",
      "id": "00229edb226bfea6e7c7a16abd6bf3126f9c427a",
      "message": "Merge pull request #546 in BC/document-impl from ~NVILLARD/document-impl:feature/AAS-590-asc-integration-emd-ticket-reassociation-after-issuance to develop\n\n* commit '9976ae4b117e4a29843741a152b989bf6d5b8503':\n  CR16071832 AAS-590 Add ServiceBEan annotation to use JCPContextProvider in CouponUtilServices",
      "parents": [
        {
          "displayId": "4c85d1f4aaf",
          "id": "4c85d1f4aaffa5c836596deeeed738fae9537f21"
        },
        {
          "displayId": "9976ae4b117",
          "id": "9976ae4b117e4a29843741a152b989bf6d5b8503"
        }
      ],
      "properties": {
        "jira-key": [
          "AAS-590"
        ],
        "records": [
          "CR 16071832"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Frederic LARUELLE",
        "emailAddress": "flaruelle@amadeus.com",
        "id": 40745,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/flaruelle"
            }
          ]
        },
        "name": "flaruelle",
        "slug": "flaruelle",
        "type": "NORMAL"
      },
      "authorTimestamp": 1582641805000,
      "committer": {
        "active": true,
        "displayName": "Frederic LARUELLE",
        "emailAddress": "flaruelle@amadeus.com",
        "id": 40745,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/flaruelle"
            }
          ]
        },
        "name": "flaruelle",
        "slug": "flaruelle",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582641805000,
      "displayId": "4c85d1f4aaf",
      "id": "4c85d1f4aaffa5c836596deeeed738fae9537f21",
      "message": "Merge pull request #545 in BC/document-impl from ~VVIEWEG/document-impl:ptr/99999999_correct_SEM_version to develop\n\n* commit 'aef0a40ebf5211b76ddf468c1843a85b93e228f8':\n  PTR 99999999: Correct SEM version",
      "parents": [
        {
          "displayId": "95e0fe8ea24",
          "id": "95e0fe8ea249174f4c1cbe5da93bb645da55f536"
        },
        {
          "displayId": "aef0a40ebf5",
          "id": "aef0a40ebf5211b76ddf468c1843a85b93e228f8"
        }
      ],
      "properties": {
        "records": [
          "PTR 99999999"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Nicolas VILLARD",
        "emailAddress": "nicolas.villard-parriault@amadeus.com",
        "id": 20912,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/nvillard"
            }
          ]
        },
        "name": "nvillard",
        "slug": "nvillard",
        "type": "NORMAL"
      },
      "authorTimestamp": 1582641293000,
      "committer": {
        "active": true,
        "displayName": "Nicolas VILLARD",
        "emailAddress": "nicolas.villard-parriault@amadeus.com",
        "id": 20912,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/nvillard"
            }
          ]
        },
        "name": "nvillard",
        "slug": "nvillard",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582641537000,
      "displayId": "9976ae4b117",
      "id": "9976ae4b117e4a29843741a152b989bf6d5b8503",
      "message": "CR16071832 AAS-590 Add ServiceBEan annotation to use JCPContextProvider in CouponUtilServices",
      "parents": [
        {
          "displayId": "5890e3bf232",
          "id": "5890e3bf2328ec9a5e08037fc8583facd5af6033"
        }
      ],
      "properties": {
        "jira-key": [
          "AAS-590"
        ],
        "records": [
          "CR 16071832"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Veit VIEWEG",
        "emailAddress": "vvieweg@amadeus.com",
        "id": 27116,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/vvieweg"
            }
          ]
        },
        "name": "vvieweg",
        "slug": "vvieweg",
        "type": "NORMAL"
      },
      "authorTimestamp": 1582641086000,
      "committer": {
        "active": true,
        "displayName": "Veit VIEWEG",
        "emailAddress": "vvieweg@amadeus.com",
        "id": 27116,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/vvieweg"
            }
          ]
        },
        "name": "vvieweg",
        "slug": "vvieweg",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582641086000,
      "displayId": "aef0a40ebf5",
      "id": "aef0a40ebf5211b76ddf468c1843a85b93e228f8",
      "message": "PTR 99999999: Correct SEM version",
      "parents": [
        {
          "displayId": "95e0fe8ea24",
          "id": "95e0fe8ea249174f4c1cbe5da93bb645da55f536"
        }
      ],
      "properties": {
        "records": [
          "PTR 99999999"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "swb2-izu-BC",
        "emailAddress": "swb2-izu-BC@amadeus.com",
        "id": 142345,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/swb2-izu-bc"
            }
          ]
        },
        "name": "swb2-izu-BC",
        "slug": "swb2-izu-bc",
        "type": "NORMAL"
      },
      "authorTimestamp": 1582621379000,
      "committer": {
        "active": true,
        "displayName": "swb2-izu-BC",
        "emailAddress": "swb2-izu-BC@amadeus.com",
        "id": 142345,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/swb2-izu-bc"
            }
          ]
        },
        "name": "swb2-izu-BC",
        "slug": "swb2-izu-bc",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582621379000,
      "displayId": "95e0fe8ea24",
      "id": "95e0fe8ea249174f4c1cbe5da93bb645da55f536",
      "message": "PTR 99999999 - Groovy Bumper - bumping version",
      "parents": [
        {
          "displayId": "e0ca7816d5f",
          "id": "e0ca7816d5fcfa45086737c05414c94494b5afc5"
        }
      ],
      "properties": {
        "records": [
          "PTR 99999999"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "authorTimestamp": 1582621329000,
      "committer": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582621329000,
      "displayId": "e0ca7816d5f",
      "id": "e0ca7816d5fcfa45086737c05414c94494b5afc5",
      "message": "Merge pull request #529 in BC/document-impl from ~NVILLARD/document-impl:feature/AAS-590-asc-integration-emd-ticket-reassociation-after-issuance to develop\n\n* commit '5890e3bf2328ec9a5e08037fc8583facd5af6033':\n  CR 16071832 AAS-590 map eTickets/EMDs coupons relations in PNRElement",
      "parents": [
        {
          "displayId": "ef1a6ed2e08",
          "id": "ef1a6ed2e08f755de8b5b8ef00ddbf530ee5c841"
        },
        {
          "displayId": "5890e3bf232",
          "id": "5890e3bf2328ec9a5e08037fc8583facd5af6033"
        }
      ],
      "properties": {
        "jira-key": [
          "AAS-590"
        ],
        "records": [
          "CR 16071832"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Nicolas VILLARD",
        "emailAddress": "nicolas.villard-parriault@amadeus.com",
        "id": 20912,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/nvillard"
            }
          ]
        },
        "name": "nvillard",
        "slug": "nvillard",
        "type": "NORMAL"
      },
      "authorTimestamp": 1574266043000,
      "committer": {
        "active": true,
        "displayName": "Nicolas VILLARD",
        "emailAddress": "nicolas.villard-parriault@amadeus.com",
        "id": 20912,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/nvillard"
            }
          ]
        },
        "name": "nvillard",
        "slug": "nvillard",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582617983000,
      "displayId": "5890e3bf232",
      "id": "5890e3bf2328ec9a5e08037fc8583facd5af6033",
      "message": "CR 16071832 AAS-590 map eTickets/EMDs coupons relations in PNRElement",
      "parents": [
        {
          "displayId": "954a223451c",
          "id": "954a223451c3193a2933a877b3744c6320949e7f"
        }
      ],
      "properties": {
        "jira-key": [
          "AAS-590"
        ],
        "records": [
          "CR 16071832"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "swb2-izu-BC",
        "emailAddress": "swb2-izu-BC@amadeus.com",
        "id": 142345,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/swb2-izu-bc"
            }
          ]
        },
        "name": "swb2-izu-BC",
        "slug": "swb2-izu-bc",
        "type": "NORMAL"
      },
      "authorTimestamp": 1582282624000,
      "committer": {
        "active": true,
        "displayName": "swb2-izu-BC",
        "emailAddress": "swb2-izu-BC@amadeus.com",
        "id": 142345,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/swb2-izu-bc"
            }
          ]
        },
        "name": "swb2-izu-BC",
        "slug": "swb2-izu-bc",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582282624000,
      "displayId": "ef1a6ed2e08",
      "id": "ef1a6ed2e08f755de8b5b8ef00ddbf530ee5c841",
      "message": "PTR 99999999 - Groovy Bumper - bumping version",
      "parents": [
        {
          "displayId": "b9714899bfb",
          "id": "b9714899bfbfe9e7631ebdbea9691636a0a6a0d8"
        }
      ],
      "properties": {
        "records": [
          "PTR 99999999"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "authorTimestamp": 1582282532000,
      "committer": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582282532000,
      "displayId": "b9714899bfb",
      "id": "b9714899bfbfe9e7631ebdbea9691636a0a6a0d8",
      "message": "Merge pull request #544 in BC/document-impl from ~JEAN.CHAUBET/document-impl:feature/ARD-14115-be-manual-tst-display-and-modify-fare-family-name-and-fare-family-owner-on-fare to develop\n\n* commit 'cf9860a1e45241ff6d54966a2055c6d7d36dc8c9':\n  CR16072991 ARD14115 add FareComponents in UpdateTst query",
      "parents": [
        {
          "displayId": "954a223451c",
          "id": "954a223451c3193a2933a877b3744c6320949e7f"
        },
        {
          "displayId": "cf9860a1e45",
          "id": "cf9860a1e45241ff6d54966a2055c6d7d36dc8c9"
        }
      ],
      "properties": {
        "jira-key": [
          "ARD-14115"
        ],
        "records": [
          "CR 16072991"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Jean-Baptiste CHAUBET",
        "emailAddress": "jean-baptiste.chaubet@amadeus.com",
        "id": 32092,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/jean.chaubet"
            }
          ]
        },
        "name": "jean.chaubet",
        "slug": "jean.chaubet",
        "type": "NORMAL"
      },
      "authorTimestamp": 1582028430000,
      "committer": {
        "active": true,
        "displayName": "Jean-Baptiste CHAUBET",
        "emailAddress": "jean-baptiste.chaubet@amadeus.com",
        "id": 32092,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/jean.chaubet"
            }
          ]
        },
        "name": "jean.chaubet",
        "slug": "jean.chaubet",
        "type": "NORMAL"
      },
      "committerTimestamp": 1582106920000,
      "displayId": "cf9860a1e45",
      "id": "cf9860a1e45241ff6d54966a2055c6d7d36dc8c9",
      "message": "CR16072991 ARD14115 add FareComponents in UpdateTst query",
      "parents": [
        {
          "displayId": "954a223451c",
          "id": "954a223451c3193a2933a877b3744c6320949e7f"
        }
      ],
      "properties": {
        "records": [
          "CR 16072991"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "swb2-izu-BC",
        "emailAddress": "swb2-izu-BC@amadeus.com",
        "id": 142345,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/swb2-izu-bc"
            }
          ]
        },
        "name": "swb2-izu-BC",
        "slug": "swb2-izu-bc",
        "type": "NORMAL"
      },
      "authorTimestamp": 1581083097000,
      "committer": {
        "active": true,
        "displayName": "swb2-izu-BC",
        "emailAddress": "swb2-izu-BC@amadeus.com",
        "id": 142345,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/swb2-izu-bc"
            }
          ]
        },
        "name": "swb2-izu-BC",
        "slug": "swb2-izu-bc",
        "type": "NORMAL"
      },
      "committerTimestamp": 1581083097000,
      "displayId": "954a223451c",
      "id": "954a223451c3193a2933a877b3744c6320949e7f",
      "message": "PTR 99999999 - Groovy Bumper - bumping version",
      "parents": [
        {
          "displayId": "e4afb74f52f",
          "id": "e4afb74f52fbb3017c9f867531de581ec5360af9"
        }
      ],
      "properties": {
        "records": [
          "PTR 99999999"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Frederic LARUELLE",
        "emailAddress": "flaruelle@amadeus.com",
        "id": 40745,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/flaruelle"
            }
          ]
        },
        "name": "flaruelle",
        "slug": "flaruelle",
        "type": "NORMAL"
      },
      "authorTimestamp": 1581083054000,
      "committer": {
        "active": true,
        "displayName": "Frederic LARUELLE",
        "emailAddress": "flaruelle@amadeus.com",
        "id": 40745,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/flaruelle"
            }
          ]
        },
        "name": "flaruelle",
        "slug": "flaruelle",
        "type": "NORMAL"
      },
      "committerTimestamp": 1581083054000,
      "displayId": "e4afb74f52f",
      "id": "e4afb74f52fbb3017c9f867531de581ec5360af9",
      "message": "Merge pull request #543 in BC/document-impl from ~VVIEWEG/document-impl:feature/cr17522604__ARD-15098__TST_update to develop\n\n* commit '25e37e1a92cc4d56e64e0fc63f115a1e48160316':\n  CR 17522604 / ARD-15098 ARD-15231 ARD-15097: Add 7 new PRA types and allow TTSTUQ 19.6 version",
      "parents": [
        {
          "displayId": "7629d571b0c",
          "id": "7629d571b0cbedf3a54d47a3cb49b02f0fbf1e6e"
        },
        {
          "displayId": "25e37e1a92c",
          "id": "25e37e1a92cc4d56e64e0fc63f115a1e48160316"
        }
      ],
      "properties": {
        "jira-key": [
          "ARD-15098",
          "ARD-15097",
          "ARD-15231"
        ],
        "records": [
          "CR 17522604"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Veit VIEWEG",
        "emailAddress": "vvieweg@amadeus.com",
        "id": 27116,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/vvieweg"
            }
          ]
        },
        "name": "vvieweg",
        "slug": "vvieweg",
        "type": "NORMAL"
      },
      "authorTimestamp": 1580403007000,
      "committer": {
        "active": true,
        "displayName": "Veit VIEWEG",
        "emailAddress": "vvieweg@amadeus.com",
        "id": 27116,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/vvieweg"
            }
          ]
        },
        "name": "vvieweg",
        "slug": "vvieweg",
        "type": "NORMAL"
      },
      "committerTimestamp": 1580403793000,
      "displayId": "25e37e1a92c",
      "id": "25e37e1a92cc4d56e64e0fc63f115a1e48160316",
      "message": "CR 17522604 / ARD-15098 ARD-15231 ARD-15097: Add 7 new PRA types and allow TTSTUQ 19.6 version",
      "parents": [
        {
          "displayId": "7629d571b0c",
          "id": "7629d571b0cbedf3a54d47a3cb49b02f0fbf1e6e"
        }
      ],
      "properties": {
        "jira-key": [
          "ARD-15098",
          "ARD-15097",
          "ARD-15231"
        ],
        "records": [
          "CR 17522604"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "authorTimestamp": 1580391409000,
      "committer": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "committerTimestamp": 1580391409000,
      "displayId": "7629d571b0c",
      "id": "7629d571b0cbedf3a54d47a3cb49b02f0fbf1e6e",
      "message": "Automatic merge from release/6.11 -> develop\n\n* commit '36bcc873315a51be43c8d323cf92e723738e4ae2':\n  PTR 18575166 - allow the exclusion of exchanged coupons in eticket association computation",
      "parents": [
        {
          "displayId": "443def88082",
          "id": "443def88082c4e566d612a1fc64762471b3a0aa7"
        },
        {
          "displayId": "36bcc873315",
          "id": "36bcc873315a51be43c8d323cf92e723738e4ae2"
        }
      ],
      "properties": {
        "records": [
          "PTR 18575166"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "authorTimestamp": 1580391408000,
      "committer": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "committerTimestamp": 1580391408000,
      "displayId": "36bcc873315",
      "id": "36bcc873315a51be43c8d323cf92e723738e4ae2",
      "message": "Merge pull request #541 in BC/document-impl from ~FPAILLET/document-impl:bugfix/ptr_18575166 to release/6.11\n\n* commit '300a7de7364617d2e7fe50529dd5150b2ea14999':\n  PTR 18575166 - allow the exclusion of exchanged coupons in eticket association computation",
      "parents": [
        {
          "displayId": "b0f4a06aa22",
          "id": "b0f4a06aa22b6532bf6447f6cda9f6b8c9e52ffd"
        },
        {
          "displayId": "300a7de7364",
          "id": "300a7de7364617d2e7fe50529dd5150b2ea14999"
        }
      ],
      "properties": {
        "records": [
          "PTR 18575166"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "authorTimestamp": 1580145314000,
      "committer": {
        "active": true,
        "displayName": "Francois PAILLET",
        "emailAddress": "Francois.PAILLET@amadeus.com",
        "id": 68064,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/fpaillet"
            }
          ]
        },
        "name": "fpaillet",
        "slug": "fpaillet",
        "type": "NORMAL"
      },
      "committerTimestamp": 1580316022000,
      "displayId": "300a7de7364",
      "id": "300a7de7364617d2e7fe50529dd5150b2ea14999",
      "message": "PTR 18575166 - allow the exclusion of exchanged coupons in eticket association computation",
      "parents": [
        {
          "displayId": "5e02d99c07b",
          "id": "5e02d99c07b25453fc2351c9fd24ebbd721c14ae"
        }
      ],
      "properties": {
        "records": [
          "PTR 18575166"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Sandrine PANTALEO",
        "emailAddress": "sandrine.pantaleo@amadeus.com",
        "id": 52154,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/spantaleo"
            }
          ]
        },
        "name": "spantaleo",
        "slug": "spantaleo",
        "type": "NORMAL"
      },
      "authorTimestamp": 1580217955000,
      "committer": {
        "active": true,
        "displayName": "Sandrine PANTALEO",
        "emailAddress": "sandrine.pantaleo@amadeus.com",
        "id": 52154,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/spantaleo"
            }
          ]
        },
        "name": "spantaleo",
        "slug": "spantaleo",
        "type": "NORMAL"
      },
      "committerTimestamp": 1580217955000,
      "displayId": "443def88082",
      "id": "443def88082c4e566d612a1fc64762471b3a0aa7",
      "message": "Automatic merge from release/6.11 -> develop\n\n* commit 'b0f4a06aa22b6532bf6447f6cda9f6b8c9e52ffd':\n  PTR18070309-com.amadeus.jcp.standard.business.reservationservice.converter.record.Tick",
      "parents": [
        {
          "displayId": "5f15524a020",
          "id": "5f15524a02012d061d73916c8e4254e854e85cc2"
        },
        {
          "displayId": "b0f4a06aa22",
          "id": "b0f4a06aa22b6532bf6447f6cda9f6b8c9e52ffd"
        }
      ],
      "properties": {
        "records": [
          "PTR 18070309"
        ]
      }
    },
    {
      "author": {
        "active": true,
        "displayName": "Sandrine PANTALEO",
        "emailAddress": "sandrine.pantaleo@amadeus.com",
        "id": 52154,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/spantaleo"
            }
          ]
        },
        "name": "spantaleo",
        "slug": "spantaleo",
        "type": "NORMAL"
      },
      "authorTimestamp": 1580217954000,
      "committer": {
        "active": true,
        "displayName": "Sandrine PANTALEO",
        "emailAddress": "sandrine.pantaleo@amadeus.com",
        "id": 52154,
        "links": {
          "self": [
            {
              "href": "https://rndwww.nce.amadeus.net/git/users/spantaleo"
            }
          ]
        },
        "name": "spantaleo",
        "slug": "spantaleo",
        "type": "NORMAL"
      },
      "committerTimestamp": 1580217954000,
      "displayId": "b0f4a06aa22",
      "id": "b0f4a06aa22b6532bf6447f6cda9f6b8c9e52ffd",
      "message": "Automatic merge from release/6.10 -> release/6.11\n\n* commit 'c8b45834cff5fbd9833648fe38dc73e09385f10c':\n  PTR18070309-com.amadeus.jcp.standard.business.reservationservice.converter.record.Tick",
      "parents": [
        {
          "displayId": "5e02d99c07b",
          "id": "5e02d99c07b25453fc2351c9fd24ebbd721c14ae"
        },
        {
          "displayId": "c8b45834cff",
          "id": "c8b45834cff5fbd9833648fe38dc73e09385f10c"
        }
      ],
      "properties": {
        "records": [
          "PTR 18070309"
        ]
      }
    }
  ]
}