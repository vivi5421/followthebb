# CHANGELOG

## v 0.5

- Double-click on version to show changelog
- Better handle wrong server responses

## v 0.4

- Highlight rows with updated/new status
- Right-click on item un-highlight it
- Support no build status (no icon instead of in progress)
- Fix notifications broken from v 0.3
- Fix minor issue (no restAPI response support)
- Remove pieces of useless code

## v 0.3

- Automatic repository configuration: now use 3 ways to detect repositories
  - All interesting Pull Requests (service provided by BitBucket
  - Recent repositories (service provided by BitBucket)
  - Repositories provided with `--repos` parameter when calling the program
- All branches are considered for each repository
- Automatically detect BitBucket password (not requested if valid in Credential Manager)
- Parameter `--wait` is now the time to wait between the end of the last scan and the beginning of the new one (instead of launching scan every X seconds)
  - Prevent multithread issues in case scanning is taking more time than provided parameter
- `--search-period` is now using the current time to check if build/PR is not too old, instead of the one when launching the program, to support case `FTBB` is launched for more than 1 day
- change images for builds
- `--request-limit` parameter put back
- `--threads` parameter to configure multithread

## v 0.2
- FFTB.pyw launcher
- pip module creation
- pip module auto-update
- request password from the UI directly
- request password until it is a right one (check password is correct). Click on Cancel leave the application.
- `--repos` file is checked from the beginning to be faster (instead of checking at each scan)
- Notification (plyer)
- Notification icons fixed
- improve logging

## v 0.1
- Auto-refresh status
- Double-click on item open in browser
- Notification (win10toast)