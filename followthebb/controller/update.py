import atexit
import copy
import functools
import itertools
import logging
import math
import multiprocessing
import operator
import re
import time

from concurrent.futures import ThreadPoolExecutor

from followthebb.controller import IS_UPDATE, IS_NEW, CONTINUE
from .requester import get_my_pull_requests, get_my_recent_repos, get_branches, get_commits

from followthebb.model.repo import Repo
from followthebb.model.repoBranch import RepoBranch

from .itemview import ItemView

logger = logging.getLogger(__name__)

def log_list(sstr, llist, sep='\n\t'):
    return sstr.format(sep.join([str(item) for item in ((['']+llist+['']) if llist else [])]))

def update(args, queue, backup):
    logger.warning('Updating...')
    _start = time.time()

    pullrequests = get_pullrequests(args)
    my_prs = get_pullrequests_author(pullrequests, args.credentials.login)

    repos = list(set(get_pullrequests_repos(pullrequests) + get_recent_repos(args) + get_args_repos(args)))
    logger.info(log_list('repos=[{}]', repos))

    repobranches = get_repobranches(args, repos)
    display_repobranches(repobranches)
    output = update_repobranches(args, my_prs, repobranches)


    result = list(sorted(output + my_prs, key=lambda x: x.timestamp, reverse=True))

    result = gener_itemview(result, backup)

    logger.warning(log_list('result=[{}]', result))
    queue.put(result)

    _end = time.time()
    logger.warning('Updating done ({})'.format(_end - _start))

def gener_itemview(result, backup):
    return [ItemView(item, backup) for item in result]


def get_pullrequests(args):
    prs = get_my_pull_requests(args)
    logger.info(log_list('prs=[{}]',prs))
    return prs

def get_pullrequests_author(prs, author):
    my_prs = [pr for pr in prs if pr.author == author]
    logger.info(log_list('my_prs=[{}]',my_prs))
    return my_prs

def get_pullrequests_repos(pullrequests):
    pullrequests_repos = [pr.repo for pr in pullrequests]
    logger.info(log_list('pullrequests_repos=[{}]', pullrequests_repos))
    return pullrequests_repos

def get_recent_repos(args):
    recent_repos = get_my_recent_repos(args)
    logger.info(log_list('recent_repos=[{}]', recent_repos))
    return recent_repos

def get_args_repos(args):
    args_repos = [repo for repo in [Repo.fromString(args, repo_str) for repo_str in args.repos] if repo.is_valid()]
    logger.info(log_list('args_repos=[{}]', args_repos))
    return args_repos

def get_repobranches(args, repos):
    return _run(args, functools.partial(get_branches, args), repos)

def update_repobranches(args, my_prs, repobranches):
    return _run(args, functools.partial(update_repobranch, args, my_prs), repobranches)

def _run(args, function, items):
    if args.threads > 0:
        with ThreadPoolExecutor(max_workers=args.threads) as tp:
            atexit.register(tp.shutdown)
            output = tp.map(function, items)
            atexit.unregister(tp.shutdown)
    else:
        output = map(function, items)
    return list(itertools.chain(*output))

def update_repobranch(args, prs, repoBranch):
    logger.info('>>> update_repobranch({})'.format(repoBranch))
    output = []
    _commits = get_commits(args, repoBranch)
    for _commit in _commits:
        if _commit.author == args.credentials.login:
            output.append(_commit)
        else: 
            match = re.match(r"^Merge pull request #(\d+) in.+$", _commit.title)
            if match and match.group(1) in [pr.id for pr in prs if pr.repo == repoBranch.repo]:
                output.append(_commit)
    logger.info('<<< update_repobranch({}): [{}]'.format(repoBranch, output))
    return output

def display_repobranches(repobranches):
    key = lambda repobranch: '{}/{}'.format(repobranch.repo.project, repobranch.repo.repo)
    log = []
    for k, v in itertools.groupby(sorted(repobranches, key=key), key=key):
        log.append(log_list('{}: [{{}}]'.format(k), [x.branch for x in v], sep='\n\t\t'))
    logger.info(log_list('items={}', log))
