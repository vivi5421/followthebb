import tkinter as tk

from .treeview import BitBucketTreeView

class FrameWithScrollbars(tk.Frame):

    def __init__(self, parent, args):
        super().__init__(parent)
        self.tree = BitBucketTreeView(self, args)
        self.horizontal_scrollbar = tk.Scrollbar(self, orient=tk.HORIZONTAL, command=self.tree.xview)
        self.horizontal_scrollbar.pack(side=tk.BOTTOM, fill=tk.X)
        self.vertical_scrollabr = tk.Scrollbar(self, orient=tk.VERTICAL, command=self.tree.yview)
        self.vertical_scrollabr.pack(side=tk.RIGHT, fill=tk.Y)
        self.tree.pack(fill=tk.BOTH, expand=True)
        self.pack(fill=tk.BOTH, expand=True)