import argparse
import getpass
import jsonpickle
import logging
import os
import re
import sys
import time

from followthebb.model.repo import Repo
from followthebb.wincred import Credentials

logger = logging.getLogger(__name__)

#DEBUG = '--debug'

####################
# ARGPARSE RELATED #
####################
def resources(folder):
  logger.debug('resources={}'.format(folder))
  return {item: os.path.join(folder, item) for item in os.listdir(folder)}

def get_options():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('--offline', action='store_true', help='Do not try to update followthebb module, use the local one directly')
  parser.add_argument('--request-limit', dest='request_limit', default=1000, help='Each BitBucket RestAPI call has a limit in the response. This is the limitation. Bigger numbers makes having less calls, but more data to process. Because of this and because you cannot provide date as input in the requests, you may have more answers than expected regarding the search-period option.')
  parser.add_argument('--search-period', dest='search_period', type=int, default=4, help='Number of days before today you want to retrieve commits and pull requests from.')
  parser.add_argument('--wait', type=int, default=60, help='Time, in seconds, between 2 scans of the repositories to find new builds and new build statuses.')
  parser.add_argument('-r', '--repos', type=str, nargs='*', default=[], help='Repositories you want to check as well, in the format "project/repo"')
  parser.add_argument('--threads', type=int, default=50, help='How many threads can be running in parallel. 0 for sequential execution')
  # logging parameters
  logging_group = parser.add_mutually_exclusive_group()
  logging_group.add_argument('--debug', action='store_true', help='Allow debug logs')
  logging_group.add_argument('--info', action='store_true', help='Allow info logs')
  # Hidden parameters
  parser.add_argument('--appname', default="FollowTheBB", help=argparse.SUPPRESS)
  parser.add_argument('--bitbucket-server', dest='bitbucket_server', default='https://rndwww.nce.amadeus.net/git', help=argparse.SUPPRESS)
  parser.add_argument('--changelog', default='https://rndwww.nce.amadeus.net/git/projects/TOOL/repos/followthebb/browse/CHANGELOG.md', help=argparse.SUPPRESS)
  parser.add_argument('--credentials', type=Credentials, default='git:https://rndwww.nce.amadeus.net', help=argparse.SUPPRESS)
  parser.add_argument('--hide-loggers', dest='hide_loggers', nargs="*", default=['chardet', 'chardet.charsetprober', 'requests', 'urllib3', 'requests.packages.urllib3'], help=argparse.SUPPRESS)
  parser.add_argument('--jsonpickle', default=jsonpickle.set_encoder_options('json', sort_keys=True, indent=2), help=argparse.SUPPRESS)
  parser.add_argument('--resources', type=resources, default=os.path.join(os.path.split(os.path.abspath(__file__))[0], 'images'), help=argparse.SUPPRESS)
  parser.add_argument('--version', help=argparse.SUPPRESS)
  return parser
