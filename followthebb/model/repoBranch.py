class RepoBranch(object):

    def __init__(self, repo, branch):
        self.repo = repo
        self.branch = branch
    
    def __str__(self):
        return '<RepoBranch: {}>'.format(repr(self))
    
    def __repr__(self):
        return '{}/{}'.format(repr(self.repo), self.branch)