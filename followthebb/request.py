import logging
import re
import requests
import time

logger = logging.getLogger(__name__)


####################
# HTTP GET METHODS #
####################
def _get(args, request):
  rq = "{args.bitbucket_server}{request}".format(**locals())
  try:
    rs = requests.get(rq, auth=(args.credentials.login, args.credentials.password))
  except:
    logger.error('No response provided for request [{}]'.format(request))
  logger.debug('_get\n>>> {rq}\n<<< status_code={rs.status_code}\n<<< {rs.text}\n'.format(**locals()))
  return rs

def _get_while(args, request, condition_to_continue=lambda _: True):
  values = []
  start = 0
  try:
    logger.debug('##### _get_while')
    while start == 0 or condition_to_continue(values[-1]): # start==0 to enter the first time  
      rs = _get(args, '{request}{mark}start={start}&limit={args.request_limit}'.format(mark='?' if not '?' in request else '&', **locals()))
      values += rs.json()['values']
      start = rs.json()['nextPageStart']
  finally:
    logger.debug('##### /_get_while')
    return values

def commit_not_too_old(args):
  return _not_too_old(args, 'committerTimestamp')

def pr_not_too_old(args):
  return _not_too_old(args, 'updatedDate')

def _not_too_old(args, field):
  return lambda item: item[field] >= (time.time() - args.search_period * 24 * 60 * 60) * 1000

def get_my_pull_requests(args):
  return _get_while(args, '/rest/api/1.0/dashboard/pull-requests', pr_not_too_old(args))

def get_my_recent_repos(args):
  return _get_while(args, '/rest/api/1.0/profile/recent/repos', lambda _: True)

def get_branches(args, project, repo):
  return _get_while(args, '/rest/api/1.0/projects/{project}/repos/{repo}/branches?ORDERBY=MODIFICATION'.format(**locals()), commit_not_too_old(args))

def get_commits(args, project, repo, branch):
  return _get_while(args, '/rest/api/1.0/projects/{project}/repos/{repo}/commits?until={branch}'.format(**locals()), commit_not_too_old(args))

def get_projects(args):
  return _get(args, '/rest/api/1.0/projects')