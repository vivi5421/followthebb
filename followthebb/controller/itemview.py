def _old(item, backup):
    try:
        return [back for back in backup if back.item == item][0]
    except (IndexError, TypeError):
        return None

class ItemView(object):

    def __init__(self, item, backup):
        self.item = item
        self.backup = backup
        self.old = _old(item, backup)
        self.is_blinking = (not self.backup == None) and (self.old == None or not self.item.build == self.old.item.build)
        self.is_highlighted = (not self.backup == None) and (self.is_blinking or self.old.is_blinking)
        self.is_notified = self.is_highlighted

    def __hash__(self):
        return self.item.hash

    def __eq__(self, other):
        return not other == None and self.item == other.item
    
    def __str__(self):
        return '<ItemView: {}>'.format(repr(self))
    
    def __repr__(self):
        return '{} [is_blinking={}] [is_highlighted={}] [is_notified={}]'.format(repr(self.item), self.is_blinking, self.is_highlighted, self.is_notified)

