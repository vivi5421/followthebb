import argparse
import getpass
import logging
import logging.config
import os
import sys
import yaml

from followthebb.view.mainapp import FollowTheBBApp
from followthebb.options import get_options

logging.basicConfig(
  level=logging.DEBUG if '--debug' in sys.argv else logging.INFO if '--info' in sys.argv else logging.WARNING,
  #level=logging.INFO,
  #level=logging.DEBUG,
  format="%(asctime)s :: %(processName)s :: %(levelname)s :: %(message)s",
  handlers=[
    logging.StreamHandler(sys.stdout),
    logging.FileHandler(os.path.join(os.getenv("TEMP"), "stderr-"+os.path.basename(sys.argv[0])), "w")
  ]
)
logger = logging.getLogger(__name__)


def main(args):
  logger.warning(args)
  logger.debug('DEBUG mode enabled')
  logger.info('INFO mode enabled')
  logger.warning('WARNING mode enabled')
  logger.error('ERROR mode enabled')
  logger.critical('CRITICAL mode enabled')
  [logging.getLogger(name).setLevel(logging.CRITICAL) for name in args.hide_loggers]
  try:
    app = FollowTheBBApp(args)
    app.mainloop()
  except:
    logger.critical('Unexpected error', exc_info=True)

if __name__ == '__main__':
  main(get_options().parse_args())
