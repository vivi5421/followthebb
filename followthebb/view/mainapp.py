import atexit
import copy
import ctypes
import logging
import multiprocessing
import queue
import tkinter as tk
import tkinter.simpledialog as tksd

from PIL import Image, ImageTk

from .scrolledFrame import FrameWithScrollbars
from .statusbar import StatusBar
from followthebb.controller.update import update
from followthebb.controller.requester import is_connexion_valid

logger = logging.getLogger(__name__)

def check_password(args):
    while not is_connexion_valid(args):
        logger.warning('Cannot connect to BitBucket RestAPI')
        password = tksd.askstring("BitBucket authentication", "Enter BitBucket password for {}:".format(args.credentials.login), show='*')
        if not password:
            logger.error('Cancel clicked when requesting password. Leaving the application')
            exit('password not provided')
        logger.warning('Can reach BitBucket RestAPI')
        args.credentials.password = password

################
#  APPLICATION #
################
class App(tk.Tk):

    def __init__(self, args, name, icon):
        super().__init__()
        self.set_icon(args, name, icon)
        self.set_title(name)
    
    def set_icon(self, args, name, icon):
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID('{}.product'.format(name))
        self.tk.call('wm', 'iconphoto', self._w, ImageTk.PhotoImage(Image.open(args.resources[icon])))
    
    def set_title(self, name):
        self.wm_title(name)


class FollowTheBBApp(App):

    def __init__(self, args):
        super().__init__(args, args.appname, 'success.ico')
        self.args = args
        check_password(args)
        self.queue = multiprocessing.Queue()
        self.lines = None
        self.frame = FrameWithScrollbars(self, args)
        self.status_bar = StatusBar(self, args)
        self.refresh()

    def refresh(self):
        self.status_bar.edit('Scanning...')
        logger.warning('Scanning...')
        p = multiprocessing.Process(target=update, args=(self.args, self.queue, self.lines))
        p.start()
        atexit.register(p.join)
        atexit.register(p.terminate)
        self._refresh_apply(p)

    def _refresh_apply(self, p):
        try:
            self.lines = self.queue.get_nowait()
            if self.frame.tree.feed(self.lines):
                # those 2 lines makes the windows pop-up on top
                self.deiconify()
                self.focus_force()
            p.terminate()
            atexit.unregister(p.terminate)
            p.join()
            atexit.unregister(p.join)
            logger.warning('Scan Completed')
            self.status_bar.edit('')
            self.after(self.args.wait * 1000, self.refresh)
        except queue.Empty:
            self.after(1000, self._refresh_apply, p)
