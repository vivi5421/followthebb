import logging

from plyer import notification

logger = logging.getLogger(__name__)


######################
# NOTIFICATION CLASS #
######################
class Notification(object):

  def __init__(self, title, description, image, duration=30):
    self.title = title
    self.description = description
    self.duration = duration
    self.image = image

  def run(self):
    notification.notify(
      title=self.title,
      message=self.description,
      app_icon=self.image,  # e.g. 'C:\\icon_32x32.ico'
      timeout=self.duration,  # seconds
    )
  
def notification_stateless(title, description, image):
  logger.info('new notification [title={}] [description={}] [image={}]'.format(title, description, image))
  Notification(title, description, image).run()







