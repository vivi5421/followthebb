import time

from followthebb.model.repo import Repo
from followthebb.model.repoBranch import RepoBranch
from followthebb.model.item import Item
import followthebb.request

def ffilter(args, items):
  return [item for item in items if item.timestamp > (time.time() - args.search_period * 24 * 60 * 60) * 1000]

def get_my_pull_requests(args):
  return ffilter(args, [Item.fromPullRequest(args, json) for json in followthebb.request.get_my_pull_requests(args)])

def get_my_recent_repos(args):
  return [Repo.fromRepo(args, json) for json in followthebb.request.get_my_recent_repos(args)]

def get_branches(args, repo):
  return [RepoBranch(repo, branch['id']) for branch in followthebb.request.get_branches(args, repo.project, repo.repo)]

def get_commits(args, repoBranch):
  return ffilter(args, [Item.fromCommit(args, json, repoBranch) for json in followthebb.request.get_commits(args, repoBranch.repo.project, repoBranch.repo.repo, repoBranch.branch)])

def is_connexion_valid(args):
    try:
        return followthebb.request.get_projects(args).status_code == 200
    except:
        return False
