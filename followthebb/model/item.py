import logging

from followthebb.model import PENDING, FAILED, SUCCESSFUL, IN_PROGRESS
from followthebb.request import _get
from .repo import Repo
from datetime import datetime
from lazy_property import LazyProperty

logger = logging.getLogger(__name__)

class Item(object):

    def __init__(self, args, type, repo, commitId, branch, author, id, displayId, title, timestamp, status, url):
        self.args = args
        self.type = type
        self.repo = repo
        self.commitId = commitId
        self.branch = branch
        self.author = author
        self.id = id
        self.displayId = displayId
        self.title = title
        self.timestamp = timestamp
        self.status = status
        self.url = url
    
    @property
    def date(self):
        return datetime.utcfromtimestamp(int(self.timestamp)/1000).strftime('%Y-%m-%d %H:%M:%S')

    @LazyProperty
    def build(self):
        try:
            _builds = [json['state'] for json in _get(self.args, '/rest/build-status/1.0/commits/{}'.format(self.commitId)).json()['values']]
            if _builds == []:
                return None
            elif any([_build == FAILED for _build in _builds]):
                return FAILED
            elif all([_build == SUCCESSFUL for _build in _builds]):
                return SUCCESSFUL
            else:
                return IN_PROGRESS
        except:
            logger.error('Cannot detect build for Item [{}]'.format(self))
            return None

    def __str__(self):
        return '<{}>'.format(repr(self))

    def __repr__(self):
        return '{self.type}: [repo={self.repo}] [branch={self.branch}] [id={self.id}] [author={self.author}] [title={self.title}] [date={self.date}] [status={self.status}] [build={self.build}]'.format(self=self)

    @classmethod
    def fromPullRequest(cls, args, json):
        try:
            return Item(
                args,
                type='PR',
                repo=Repo.fromPullRequest(args, json),
                commitId=json['fromRef']['latestCommit'],
                branch=json['toRef']['displayId'],
                author=json['author']['user']['slug'],
                id=str(json['id']),
                displayId=json['id'],
                title=json['title'],
                timestamp=json['updatedDate'],
                status=json['state'],
                url=json["links"]["self"][0]["href"]
            )
        except:
            logger.critical('item.fromPullRequest()\nargs=[{args}]\njson=[{json}]'.format(**locals()), exc_info=True)
    
    @classmethod
    def fromCommit(cls, args, json, repoBranch):
        try:
            repo = repoBranch.repo
            id=json['id']
            return Item(
                args,
                type='COMMIT',
                repo=repo,
                commitId=id,
                branch=repoBranch.branch,
                author=json['author']['name'],
                id=id,
                displayId=json['displayId'],
                title=json['message'].split('\n')[0],
                timestamp=json['committerTimestamp'],
                status='',
                url='{args.bitbucket_server}/projects/{repo.project}/repos/{repo.repo}/commits/{id}'.format(**locals())
            )
        except:
            logger.critical('item.fromCommit()\nargs=[{args}]\njson=[{json}]\nrepoBranch=[{repoBranch}]'.format(**locals()), exc_info=True)

    def __hash__(self):
        return self.id
    
    def __eq__(self, other):
        return not other == None and self.repo == other.repo and self.branch == other.branch and self.id == other.id