import logging
import traceback

from followthebb.request import _get

logger = logging.getLogger(__name__)

class Repo(object):

    def __init__(self, args, project, repo):
        self.args = args
        self.project = project
        self.repo = repo

    def is_valid(self):
        try:
            return _get(self.args, '/rest/api/1.0/projects/{self.project}/repos/{self.repo}'.format(**locals())).status_code == 200 
        except:
            logger.error('Cannot retrieve response for Repo [{}]'.format(self))
            return False

    @classmethod
    def fromPullRequest(cls, args, json):
        return Repo(
            args=args,
            project=json['toRef']['repository']['project']['key'],
            repo=json['toRef']['repository']['slug']
        )

    
    @classmethod
    def fromCommit(cls, args, json):
        return Repo(
            args=args,
            project=json['project']['key'],
            repo=json['slug']
        )
    
    @classmethod
    def fromRepo(cls, args, json):
        return Repo.fromCommit(args, json)
    
    @classmethod
    def fromString(cls, args, text):
        return Repo(args, *text.split('/'))

    def __str__(self):
        return '<Repo: {}>'.format(repr(self))
    
    def __repr__(self):
        return '{}/{}'.format(self.project, self.repo)
    
    def __hash__(self):
        return 1

    def __eq__(self, other):
        return not other == None and self.project == other.project and self.repo == other.repo
