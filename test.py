# import tkinter as tk
# import time
# root = tk.Tk()
# text = tk.Text(root)
# text.pack()
# text.insert('1.0', "line 1\nline 2\nline 3\n")
# text.tag_add('bg', '1.0', '1.4')
# text.tag_config('bg', background='red')
# root.update()
# time.sleep(1)
# text.tag_add('bg', '2.0', '2.4')
# text.tag_config('bg', background='blue')
# root.update()
# input('v')

# -*- coding: utf-8 -*-
import tkinter
from tkinter import ttk

def fixed_map(option):
    # Fix for setting text colour for Tkinter 8.6.9
    # From: https://core.tcl.tk/tk/info/509cafafae
    #
    # Returns the style map for 'option' with any styles starting with
    # ('!disabled', '!selected', ...) filtered out.

    # style.map() returns an empty list for missing options, so this
    # should be future-safe.
    return [elm for elm in style.map('Treeview', query_opt=option) if
      elm[:2] != ('!disabled', '!selected')]

root = tkinter.Tk ()

style = ttk.Style()
style.map('Treeview', foreground=fixed_map('foreground'), background=fixed_map('background'))
#style.configure ("Treeview", foreground="yellow", background="grey", fieldbackground="green")
tree = ttk.Treeview (root, columns=('Data'))
tree.heading ('#0', text='Item')
tree.heading ('#1', text='Data')
tree.insert ("", "end", text="Item_0", values=100, tags="A")
tree.insert ("", "end", text="Item_1", values=200, tags="B")
tree.insert ("", "end", text="Item_2", values=300, tags="C")
tree.tag_configure ("A", foreground="black") #Py 3.7.3: no effect
tree.tag_configure ("B", foreground="red")
tree.pack ()
root.mainloop ()

# import tkinter
# root = tkinter.Tk()
# print(root.tk.call('info', 'patchlevel'))
