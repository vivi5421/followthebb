import logging
import multiprocessing
import sys

logging.basicConfig(
	level=logging.DEBUG if '--debug' in sys.argv,
    format='%(asctime)s :: %(message)s',
    handlers=[
		logging.FileHandler("example1.log") if '--log-file' in sys.argv else logging.NullHandler(),
		logging.StreamHandler()
	]
)

def count(i):
	time.sleep(3)
	logging.debug(i)