import logging
import webbrowser

import tkinter as tk

logger = logging.getLogger(__name__)


##############
# STATUS BAR #
##############
class StatusBar(tk.Frame):   
  def __init__(self, parent, args):
      super().__init__(parent)
      self.args = args
      self.variable = tk.StringVar()        
      versionLabel = tk.Label(self, bd=1, relief=tk.SUNKEN, anchor=tk.W, text='version: {}'.format(args.version), font=('arial',10,'normal'))
      versionLabel.pack(side=tk.LEFT)
      versionLabel.bind('<Double-1>', self.changelog)
      self.label = tk.Label(self, bd=1, relief=tk.SUNKEN, textvariable=self.variable, font=('arial',10,'normal'))
      self.variable.set('Initial Scan... {}'.format(args.version))
      self.label.pack(side=tk.LEFT)
      self.pack(side=tk.BOTTOM, fill=tk.X)

  def edit(self, text):
    self.variable.set(text)
    self.label.pack()

  def changelog(self, event):
    logger.warning('Webbrowser for changelog')
    webbrowser.open(self.args.changelog, new=2)


